<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requestedd.
|
*/

use Firebase\JWT\JWT;

$router->get('/listsubject', function () {
    $results = app('db')->select("SELECT * FROM score");
    return response()->json($results);
});

$router->post('/addsubject', ['middleware'=>'auth', function(Illuminate\Http\Request $request) { //add score

    $user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM userstudent WHERE UserID=?",
								  [$user_id]);
	
    if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {

		$mid_term = $request->input("mid_term");

        $fi_nal = $request->input("fi_nal");

        $to_tal = $request->input("to_tal");

        $query = app ('db')->insert('INSERT into score
                                        (ScoreMid, ScoreFin, Total)
                                        VALUES(?,?, ?)',
                                            [$mid_term, $fi_nal,
                                             $to_tal]);
    return "Ok";
	}

}]);

$router->put('/updatescore', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

    $user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM userstudent WHERE UserID=?",
								  [$user_id]);
	
    if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {

        //$reg_id = $request->input("reg_id");

		$subject_id = $request->input("subject_id");

        $mid_term = $request->input("mid_term");

        $fi_nal = $request->input("fi_nal");

        $to_tal = $request->input("to_tal");


        $query = app ('db')->update('UPDATE score
                                        SET ScoreMid=?,
                                            ScoreFin=?,
                                            Total=?
                                            
                                        WHERE
                                            RegID=?',
                                        [   $mid_term,
                                            $fi_nal,
                                            $to_tal,
                                            $subject_id]);
    return "Ok";
	}

}]);

$router->delete('/deletesubject', function (Illuminate\Http\Request $request) {

    $user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM userstudent WHERE UserID=?",
								  [$user_id]);
	
    if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		$subject_id = $request->input("subject_id");

        $query = app('db')->delete('DELETE FROM score
                                    WHERE
                                        RegID=?',
                                        [$subject_id]);
    return "Ok";
		
	}
});

$router->get('/list_score',function() {
    
    $results =app('db')->select("SELECT UserID,
                                        userstudent.StuID,
                                        userstudent.name,
                                        userstudent.surname,
                                        score.ScoreMid,
                                        score.ScoreFin,
                                        score.Total
                                FROM score, userstudent
                                WHERE score.RegID = userstudent.RegID");

    return response()->json($results);
});



$router->post('/register', function(Illuminate\Http\Request $request) {

    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));
    $name = $request->input("name");
    $surname = $request->input("surname");
    $studentid= $request->input("studentid");

    $query = app('db')->insert('INSERT into userstudent
                                    (username, password, StuID, name, surname,RegID, IsAdmin)
                                    VALUES (?, ?, ?, ?, ?, ?, ?)',
                                     [ $username,
                                       $password,
                                       $studentid,
                                       $name,
                                       $surname,
                                       '0',
                                       'n' ]);
    
    return "Ok";
});

$router->post('/login',function(Illuminate\Http\Request $request){

    $username = $request->input("username");
    $password = $request->input("password");

    $result = app('db')->select('SELECT UserID, password, IsAdmin from userstudent WHERE username=?',
                                        [$username]);
    
    $loginResult = new stdClass();

    if(count($result)==0){
        $loginResult->status = "fail";
        $loginResult->reason = "User is not founded";

    }else {
        if(app('hash')->check($password, $result[0]->password)){
            $loginResult->status = "success";

            $payload = [
                'iss'=>"score_student_system",//Issuer of the token
                'sub' => $result[0]->UserID,//Subject of the token
                'iat' => time(),
                'exp' => time() +30*60*60,
            ];

            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            $loginResult->isAdmin = $result[0]->IsAdmin;
            
            return response()->json($loginResult);

        }else{
            $loginResult->status= "fail";
            $loginResult->reason = "Incoorrect Password";
            return response()->json($loginResult);
        }
    }

    return response()->json($loginResult);
});

$router->get('/get_user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT * FROM userstudent
							      WHERE (userstudent.UserID=?)',
										[$user_id]);
	return response()->json($results);
}]);


