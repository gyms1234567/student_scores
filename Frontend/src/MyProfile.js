import {
  Link,
  Redirect
} from "react-router-dom";


import { useState, useEffect } from 'react';
import axios from 'axios';
import './Myprofile.css'
import { Component } from "react";


function MyProfile(){

	const [name, 	setname] 	= useState("");
	const [surname, setsurname] = useState("");
	const [stuid,  	setStuID] 	= useState("");
	
	
	const [hasToken, setHasToken] = useState(true);	
	useEffect (()=>{
		
		
		if (sessionStorage.getItem('user_api_token')==null) {
			setHasToken(false);
		}else {
			axios.get('http://localhost/api/v1/get_user_profile?api_token='+sessionStorage.getItem('user_api_token'),
			).then (
				res=> {	
					setname(res.data[0].name);
					setsurname(res.data[0].surname);
					setStuID(res.data[0].StuID);
				}
			);
			
			
		
		}
	}, []);

	return ( 
	<div>
	
		{!hasToken && <Redirect to="/" /> }

	
			
		<p className="infor">I N F O R M A T I O N</p>

	
				<div className="flex-items-2">
					<p className="name">{name}  {surname}  {stuid}</p>
				</div>
	
	

	
			
	
	</div>);
	
}

export default MyProfile;