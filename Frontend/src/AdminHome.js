import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import AdminAddScore from './AdminAddScore';
import AdminConfirmRegistration from './AdminConfirmRegistration';

import { useState, useEffect } from 'react';
import './AdminHome.css'



function AdminHome() {
	
	const [hasToken, setHasToken] = useState(true);	

	
	function logout() {
		//ระบบ Logout ในระบบ Stateless Authentication นั้น ไม่จำเป็นต้องยิง REST API
		//เพียงเคลียร์ token ออกจาก Memory แล้ว redirect กลับหน้า Login เป็นอันเพียงพอ
		
		sessionStorage.clear();
		setHasToken(false);
		alert("Logged Out");
	}
	return (<div>
	
			
		
		<p className="headd"> WELCOME SCORES SYSTEMS</p>


	
			
	
		<h3> 
		<Link to="/admin"> Admin</Link> /  
			<Link to="/admin/addscore"> เพิ่มคะแนน </Link> /  
			 <Link to="/admin/confirm">  อนุมัติการสมัคร </Link> /  
			 <button onClick={ ()=>{logout()}}> ล็อคเอาท์  </button>
		</h3>
			<div className="yai">
			<button className="edit"><Link to="/admin/addscore">Edit</Link></button>
			<button className="save">Save</button>
		</div>
		
			<table className="score_table">
				<tr>
					<th>รหัสนักศึกษา</th>
					<th>ชื่อ - นามสกุล</th>
					<th>คะแนนกลางภาค</th>
					<th>คะแนนปลายภาค</th>
					<th className="total">รวม</th>
				</tr>
				<tr>
					<td>6210210159</td>
					<td>Suwimon Sithai</td>
					<td>50</td>
					<td>50</td>
					<td>100</td>
				</tr>
				<tr>
					<td>6210210522</td>
					<td>Sanantinee Tanakorum</td>
					<td></td>
					
					<td></td>
					<td></td>
				</tr>

				<tr>
					<td>6210210491</td>
					<td>Phatcharawat Rattanasutthisak</td>
					<td>51</td>
					<td>49</td>
					<td>100</td>
				</tr>
			</table>

		{!hasToken && <Redirect to="/" /> }
		<hr />
		
			<Switch>
				<Route path="/admin/addscore/">
					<AdminAddScore />
				</Route>
				
				
				<Route path="/admin/confirm/">
					<AdminConfirmRegistration />
				</Route>
				
				
			</Switch>
				
	
	
	</div>)

}
export default AdminHome;