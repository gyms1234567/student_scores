
import { useState } from 'react';
import axios from 'axios';


import {
	Link,
	Redirect
} from "react-router-dom";
import './Signup.css'

function SignUp() {

	const [name, 	setName] 	= useState("");
	const [surname, setSurname] = useState("");
	const [studentid, setstudentid] = useState("");
	const [username,setUsername]= useState("");
	const [pw1,  	setPw1] 	= useState("");
	const [pw2,  	setPw2] 	= useState("");
	
	const [isSuccess, setIsSuccess] = useState(false);
	function sendSignup(){
			
		if (pw1 != pw2) {			
			alert ("Password ไม่ตรงกัน กรุณากรอกใหม่");
		}else{
		
			axios.post('http://localhost:8000/register',
				{
					"name" : name,
					"surname" : surname,
					"studentid" : studentid,
					"username" : username,
					"password" : pw1,
				}
			).then (
				res=> {	
					console.log(res.data);
					if (res.data == "Ok") {
							alert("สมัคร สำเร็จ");
							setIsSuccess(true)
					}else {
							alert ("เกิดปัญหา สมัครไม่ได้");
							setIsSuccess(false)
					}
				}
			);
				
		}
		
	}
	
	
	return (<div>
			<div className="container1">
				<h1>Welcome : This is signup page </h1>
			<button className="signin1"><Link to="/">SIGNIN</Link></button><button className="signup1">SIGNUP</button>
			<div className="form-box1">
		     	<h2> StudentID <input type="text"  value={studentid}			onChange={(e)=>{setstudentid(e.target.value)}}	/ > </h2>
				<h2> Name     <input type="text" value={name} 					onChange={(e)=>{setName(e.target.value)}}	/ > </h2>
				<h2> Surname  <input type="text" value={surname}				onChange={(e)=>{setSurname(e.target.value)}}	/ > </h2>
				<h2> Username <input type="text"  value={username}			onChange={(e)=>{setUsername(e.target.value)}}	/ > </h2>
				<h2> Password <input type="password"  value={pw1}			onChange={(e)=>{setPw1(e.target.value)}}	/ > </h2>
				<h2> Confirm Password <input type="password" value={pw2}	onChange={(e)=>{setPw2(e.target.value)}}  / > </h2>
			</div>
				<button className="ok" onClick={()=>sendSignup()}>SIGNUP</button>
			
			{isSuccess && <Redirect to="/" /> }
			</div>
	</div>);
	
}

export default SignUp;