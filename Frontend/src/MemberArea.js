

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
   Redirect
} from "react-router-dom";

import RegisterEvent from "./RegisterEvent";
import ListMyEvent from "./ListMyEvent";
import MyProfile from "./MyProfile";
import './MemberArea.css'

import { useState, useEffect } from 'react';


function MemberArea(props) {

	const [hasToken, setHasToken] = useState(true);	

	
	
	function logout() {
		//ระบบ Logout ในระบบ Stateless Authentication นั้น ไม่จำเป็นต้องยิง REST API
		//เพียงเคลียร์ token ออกจาก Memory แล้ว redirect กลับหน้า Login เป็นอันเพียงพอ
		
		sessionStorage.clear();
		setHasToken(false);
		alert("Logged Out");
	}
	
	return ( 
	<div> 
		<h1></h1>
		
		<h3>
			<nav> 
				<label className="logo">SCORE</label>
				<ul>
					 
					 <li><Link to="/member/register"><a></a></Link></li> 
					 <li><Link to="/member/myevent"><a>Score</a></Link></li> 
					 <li><Link to="/member/myprofile"><a>Profile</a> </Link></li> 
					 <button className="logout" onClick={()=>{ logout() }}>LOGOUT</button>  
				 </ul> 
				
			</nav>
			
			
		</h3>
		<hr />
	
		{!hasToken && <Redirect to="/" /> }
		
		<Switch>
			<Route exact path="/"> 
			</Route>
			<Route path="/member/register/">
				<RegisterEvent />
			</Route>
			
			<Route path="/member/myevent/">
				<ListMyEvent />
			</Route>
			
			<Route path="/member/myprofile/">
				<MyProfile />
			</Route>
	

		</Switch>
	</div>		
	);

}

export default MemberArea;