import {
  Link,
  Redirect
} from "react-router-dom";

import { useState, useEffect } from 'react';
import axios from 'axios';
import AdminConfirmRegistration from './AdminConfirmRegistration';
import './AddminAddEvent.css'


function AdminAddEvent() {

	const [hasToken, setHasToken] = useState(true);	
	
	
	const [eventName, 	setEventName] 	= useState("");
	const [eventDate, 	setEventDate] 	= useState("");
	const [eventPlace,  setEventPlace] 	= useState("");
	
	useEffect (()=>{
			
		if (sessionStorage.getItem('admin_api_token')==null) {
			setHasToken(false);
		}
	}, []);
	
	
	function sendAddEvent(){
		axios.post('http://localhost/api/v1/admin_add_event',
				{
					'api_token': sessionStorage.getItem('admin_api_token'),
					'event_name' : eventName,
					'event_datetime' : eventDate,
					'event_place' : eventPlace,
					
				}
			).then (
				res=> {	
					
					if (res.data == "Ok") {
							alert("เพิ่มสำเร็จ");
					}else {
							alert ("เกิดปัญหา เพิ่มไม่ได้");
					}
				}
			);
	}
	
	
	return (<div>
			{!hasToken && <Redirect to="/" /> }

				
				<h2> คะแนนกลางภาค <input className="mid" type="text"  				value={eventName} 	onChange={(e)=>setEventName(e.target.value)}	/ > </h2>
				<h2> สถานที่: <input type="text" 				value={eventPlace}  onChange={(e)=>setEventPlace(e.target.value)} 	/ > </h2>
			
				<button onClick={()=>{sendAddEvent()}}>ส่งข้อมูล</button>
				
				

<input className="" type="text" value={eventName} 	onChange={(e)=>setEventName(e.target.value)}	/ >
			</div>);
	
		
}

export default AdminAddEvent;