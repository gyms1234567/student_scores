-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for score_student_system
CREATE DATABASE IF NOT EXISTS `score_student_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `score_student_system`;

-- Dumping structure for table score_student_system.score
CREATE TABLE IF NOT EXISTS `score` (
  `RegID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ScoreMid` int(10) unsigned NOT NULL DEFAULT '0',
  `ScoreFin` int(10) unsigned NOT NULL DEFAULT '0',
  `Total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RegID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table score_student_system.score: ~0 rows (approximately)
/*!40000 ALTER TABLE `score` DISABLE KEYS */;
/*!40000 ALTER TABLE `score` ENABLE KEYS */;

-- Dumping structure for table score_student_system.userstudent
CREATE TABLE IF NOT EXISTS `userstudent` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RegID` int(11) unsigned NOT NULL,
  `StuID` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `IsAdmin` char(50) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table score_student_system.userstudent: ~0 rows (approximately)
/*!40000 ALTER TABLE `userstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `userstudent` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
